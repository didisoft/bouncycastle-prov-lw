package lw.bouncycastle.crypto.ec;

import lw.bouncycastle.crypto.CipherParameters;
import lw.bouncycastle.math.ec.ECPoint;

public interface ECDecryptor
{
    void init(CipherParameters params);

    ECPoint decrypt(ECPair cipherText);
}
