package lw.bouncycastle.crypto.generators;

import lw.bouncycastle.crypto.AsymmetricCipherKeyPair;
import lw.bouncycastle.crypto.AsymmetricCipherKeyPairGenerator;
import lw.bouncycastle.crypto.EphemeralKeyPair;
import lw.bouncycastle.crypto.KeyEncoder;

public class EphemeralKeyPairGenerator
{
    private AsymmetricCipherKeyPairGenerator gen;
    private KeyEncoder keyEncoder;

    public EphemeralKeyPairGenerator(AsymmetricCipherKeyPairGenerator gen, KeyEncoder keyEncoder)
    {
        this.gen = gen;
        this.keyEncoder = keyEncoder;
    }

    public EphemeralKeyPair generate()
    {
        AsymmetricCipherKeyPair eph = gen.generateKeyPair();

        // Encode the ephemeral public key
         return new EphemeralKeyPair(eph, keyEncoder);
    }
}
