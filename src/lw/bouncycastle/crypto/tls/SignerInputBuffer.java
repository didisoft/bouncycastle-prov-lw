package lw.bouncycastle.crypto.tls;

import java.io.ByteArrayOutputStream;

import lw.bouncycastle.crypto.Signer;

class SignerInputBuffer extends ByteArrayOutputStream
{
    void updateSigner(Signer s)
    {
        s.update(this.buf, 0, count);
    }
}