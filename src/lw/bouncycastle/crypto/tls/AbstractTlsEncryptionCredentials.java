package lw.bouncycastle.crypto.tls;

public abstract class AbstractTlsEncryptionCredentials
    extends AbstractTlsCredentials
    implements TlsEncryptionCredentials
{
}
