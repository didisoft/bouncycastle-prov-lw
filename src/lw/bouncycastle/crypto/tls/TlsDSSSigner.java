package lw.bouncycastle.crypto.tls;

import lw.bouncycastle.crypto.DSA;
import lw.bouncycastle.crypto.params.AsymmetricKeyParameter;
import lw.bouncycastle.crypto.params.DSAPublicKeyParameters;
import lw.bouncycastle.crypto.signers.DSASigner;
import lw.bouncycastle.crypto.signers.HMacDSAKCalculator;

public class TlsDSSSigner
    extends TlsDSASigner
{
    public boolean isValidPublicKey(AsymmetricKeyParameter publicKey)
    {
        return publicKey instanceof DSAPublicKeyParameters;
    }

    protected DSA createDSAImpl(short hashAlgorithm)
    {
        return new DSASigner(new HMacDSAKCalculator(TlsUtils.createHash(hashAlgorithm)));
    }

    protected short getSignatureAlgorithm()
    {
        return SignatureAlgorithm.dsa;
    }
}
