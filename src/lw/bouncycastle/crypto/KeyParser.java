package lw.bouncycastle.crypto;

import java.io.IOException;
import java.io.InputStream;

import lw.bouncycastle.crypto.params.AsymmetricKeyParameter;

public interface KeyParser
{
    AsymmetricKeyParameter readKey(InputStream stream)
        throws IOException;
}
