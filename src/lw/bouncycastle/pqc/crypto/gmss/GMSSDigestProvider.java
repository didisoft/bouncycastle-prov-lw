package lw.bouncycastle.pqc.crypto.gmss;

import lw.bouncycastle.crypto.Digest;

public interface GMSSDigestProvider
{
    Digest get();
}
