package lw.bouncycastle.asn1.test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Date;

import lw.bouncycastle.asn1.ASN1Boolean;
import lw.bouncycastle.asn1.ASN1Enumerated;
import lw.bouncycastle.asn1.ASN1InputStream;
import lw.bouncycastle.asn1.ASN1Integer;
import lw.bouncycastle.asn1.ASN1ObjectIdentifier;
import lw.bouncycastle.asn1.ASN1OutputStream;
import lw.bouncycastle.asn1.ASN1Primitive;
import lw.bouncycastle.asn1.BERConstructedOctetString;
import lw.bouncycastle.asn1.BERSequence;
import lw.bouncycastle.asn1.BERSet;
import lw.bouncycastle.asn1.BERTaggedObject;
import lw.bouncycastle.asn1.DERApplicationSpecific;
import lw.bouncycastle.asn1.DERBMPString;
import lw.bouncycastle.asn1.DERBitString;
import lw.bouncycastle.asn1.DERGeneralString;
import lw.bouncycastle.asn1.DERGeneralizedTime;
import lw.bouncycastle.asn1.DERIA5String;
import lw.bouncycastle.asn1.DERNull;
import lw.bouncycastle.asn1.DERNumericString;
import lw.bouncycastle.asn1.DEROctetString;
import lw.bouncycastle.asn1.DERPrintableString;
import lw.bouncycastle.asn1.DERSequence;
import lw.bouncycastle.asn1.DERSet;
import lw.bouncycastle.asn1.DERT61String;
import lw.bouncycastle.asn1.DERTaggedObject;
import lw.bouncycastle.asn1.DERUTCTime;
import lw.bouncycastle.asn1.DERUTF8String;
import lw.bouncycastle.asn1.DERUniversalString;
import lw.bouncycastle.asn1.DERVisibleString;
import lw.bouncycastle.util.test.SimpleTestResult;
import lw.bouncycastle.util.test.Test;
import lw.bouncycastle.util.test.TestResult;

public class EqualsAndHashCodeTest
    implements Test
{
    public TestResult perform()
    {
        byte[]    data = { 0, 1, 0, 1, 0, 0, 1 };
        
        ASN1Primitive    values[] = {
                new BERConstructedOctetString(data),
                new BERSequence(new DERPrintableString("hello world")),
                new BERSet(new DERPrintableString("hello world")),
                new BERTaggedObject(0, new DERPrintableString("hello world")),
                new DERApplicationSpecific(0, data),
                new DERBitString(data),
                new DERBMPString("hello world"),
                new ASN1Boolean(true),
                new ASN1Boolean(false),
                new ASN1Enumerated(100),
                new DERGeneralizedTime("20070315173729Z"),
                new DERGeneralString("hello world"),
                new DERIA5String("hello"),
                new ASN1Integer(1000),
                new DERNull(),
                new DERNumericString("123456"),
                new ASN1ObjectIdentifier("1.1.1.10000.1"),
                new DEROctetString(data),
                new DERPrintableString("hello world"),
                new DERSequence(new DERPrintableString("hello world")),
                new DERSet(new DERPrintableString("hello world")),
                new DERT61String("hello world"),
                new DERTaggedObject(0, new DERPrintableString("hello world")),
                new DERUniversalString(data),
                new DERUTCTime(new Date()),
                new DERUTF8String("hello world"),
                new DERVisibleString("hello world")
            };
        
        try
        {
            ByteArrayOutputStream   bOut = new ByteArrayOutputStream();
            ASN1OutputStream        aOut = new ASN1OutputStream(bOut);
            
            for (int i = 0; i != values.length; i++)
            {
                aOut.writeObject(values[i]);
            }
            
            ASN1Primitive[] readValues = new ASN1Primitive[values.length];
            
            ByteArrayInputStream    bIn = new ByteArrayInputStream(bOut.toByteArray());
            ASN1InputStream         aIn = new ASN1InputStream(bIn);
            
            for (int i = 0; i != values.length; i++)
            {
                ASN1Primitive o = aIn.readObject();
                if (!o.equals(values[i]))
                {
                    return new SimpleTestResult(false, getName() + ": Failed equality test for " + o.getClass());
                }
                
                if (o.hashCode() != values[i].hashCode())
                {
                    return new SimpleTestResult(false, getName() + ": Failed hashCode test for " + o.getClass());
                }
            }
        }
        catch (Exception e)
        {
            return new SimpleTestResult(false, getName() + ": Failed - exception " + e.toString(), e);
        }
        
        return new SimpleTestResult(true, getName() + ": Okay");
    }

    public String getName()
    {
        return "EqualsAndHashCode";
    }

    public static void main(
        String[] args)
    {
        EqualsAndHashCodeTest    test = new EqualsAndHashCodeTest();
        TestResult      result = test.perform();

        System.out.println(result);
    }
}
