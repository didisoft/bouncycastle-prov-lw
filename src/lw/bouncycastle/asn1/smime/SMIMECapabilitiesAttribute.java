package lw.bouncycastle.asn1.smime;

import lw.bouncycastle.asn1.DERSequence;
import lw.bouncycastle.asn1.DERSet;
import lw.bouncycastle.asn1.cms.Attribute;

public class SMIMECapabilitiesAttribute
    extends Attribute
{
    public SMIMECapabilitiesAttribute(
        SMIMECapabilityVector capabilities)
    {
        super(SMIMEAttributes.smimeCapabilities,
                new DERSet(new DERSequence(capabilities.toASN1EncodableVector())));
    }
}
