-------------------------------------------------------
* DidiSoft Inc Eood, 2016, www.didisoft.com
-------------------------------------------------------

This project is originated from the source code of the BouncyCastle Java library, 
but the package names were renamed from org.bouncycastle.* to lw.bouncycastle.*

DidiSoft OpenPGP Library for Java use these packages in order to avoid class loading collisions with 
older versions of the BouncyCastle Java library.